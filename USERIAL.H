#include<stdio.h>
#include<string.h>

#include "Global.h"
#include "functions.h"

//UNIDAD PARA EL MANEJO DEL PUERTO SERIE, USANDO INTERUPCIONES


//USES Dos{uCharLis,uVeritab}; 

   //COMUNICACION IO
   unsigned short int DataIn[1928];
   short int DataOut[1064];

class Puerto{
     Pointer Com1 ; //GUARDA LA DIRECCION DEL PROCEDIMIENTO ORIGINAL
     Pointer Com2 ; //GUARDA LA DIR DEL VECTOR ORIGINAL DEL COM2
     short int PuertoCom; //QUE PUERTO USAR 1:COM1,2:COM2
     short int Holdint;
     /*bool modelos;
     char SnombreArchivo[14];*/
     void Inicializa(short int nPuerto; long int baudios); //INICIALIZA EL PUERTO Y EL VECTOR DE INTERUP
     bool Envia(char c); // ENVIA UN CARACTER AL PUERTO SERIE
     void finalizar(); //RESTABLECE LOS VECTORES ORIGINALES Y LIMPIA LA MEM
//     void Borra(); //LIMPIA LA LISTA
     void EstableceRTS(bool como); //PONE EN UNO LA LINEA DE RTS DEL UART
     bool HayDatos(); //TRUE SI LA LISTA YA TIENE DATOS VALIDOS
//     bool PasaARchivo(); //PONE LA LISTA EN UN ARCHIVO
     void EnviaCadena(char *cadena);
     char ConvCadena();
     bool CTS();
//     bool modelosDatYa();
}PuertoSerial;

  unsigned short int Indiser;
  DataOut ListSer;
  int Bads;
  long int Cars;
  unsigned short int Base; //DIRECCION BASE DEL UART
  short int recibi; //GUARDA LO QUE SE RECIBE DEL PUERTO
  short int Estado; //ESPECIFICA SI YA SE HA COMPLETADO UN MENSAJE
  bool RTS_Prendido;

/*shor int foco;
  char DriveX[5];
  void QueDriveUsar();
  void DImprime();*/

  DataIn DatRec;
  DataOut DatEnv; 

  short int checkdriver;
  bool  PortAlin();
  char  EnviaLinea(char *Linea, shor int Mode);
  char  EnvRecCadena(char *St);
  char  RecibeLinea(shor int Mode);

//===========================================================

bool PortAlin()
{
	unsigned short int T1;
    short int L1;

   PortAlin = false;

   /*if (RS232)
   {
      if (Iors232)
      {
         asm{
                 mov es,DirPort;
                 mov bl,NumLec;
                 mov di,Bite1;
                 mov ah,Bit1;
            @lec:mov al,es:[di];
                 and al,ah;
                 jnz @fin;
                 dec bl;
                 jnz @lec;
            @fin:mov L1,bl;
         }
         if (L1 > 0) 
         {
            PortAlin = true;
            asm{ cli; }
         }
      } else
      {
         asm{
                 mov es,DirPort;
                 mov ah,NumVal;
                 mov di,Bite1;
                 mov bh,Bit1;
            @lec:mov al,es:[di];
                 and al,bh;
                 jnz @dec;
                 mov ah,1;
            @dec:dec ah;
                 jnz @lec;
                 mov L1,al;
         }
         if (L1 == 0) Iors232 = true;
      }
      exit(0);
   }*/

   if (RedActiva) 
   {
      asm{
              mov es,DirPort;
              mov ah,NumLec;
              mov bl,NumVal;
              mov di,Bite1;
              mov bh,Bit1;
         @lec:mov al,es:[di];
              and al,bh;
              jnz @dec;
              dec bl;
              jnz @lec;
              mov ah,1;
         @dec:dec ah;
              jnz @lec;
              mov L1,al;
      }
      if (L1 > 0)
      {
         PortAlin = true;
         IORAC = IORED1;
         Bite = Bite1;
         Bit = Bit1;
         exit(0);
      }
   } else
   if (Bit1 > 0)
   {
      asm{
              mov es,DirPort;
              mov ah,NumVal;
              mov di,Bite1;
              mov bh,Bit1;
         @lec:mov al,es:[di];
              and al,bh;
              jnz @dec;
              mov ah,1;
         @dec:dec ah;
              jnz @lec;
              mov L1,al;
      }
      if (L1 == 0) RedActiva = true;
   }

   if (IoActivo)
   {
      asm{
              mov es,DirPort;
              mov ah,NumLec;
              mov bl,NumVal;
              mov di,Bite2;
              mov bh,Bit2;
         @lec:mov al,es:[di];
              and al,bh;
              jnz @dec;
              dec bl;
              jnz @lec;
              mov ah,1;
         @dec:dec ah;
              jnz @lec;
              mov L1,al;
      }
      if (L1 > 0)
      {
         PortAlin = true;
         IORAC = IORED2;
         Bite = Bite2;
         Bit = Bit2;
         exit(0);
      }
   } else
   if (Bit2 > 0)
   {
      asm{
              mov es,DirPort;
              mov ah,NumVal;
              mov di,Bite2;
              mov bh,Bit2;
         @lec:mov al,es:[di];
              and al,bh;
              jnz @dec;
              mov ah,1;
         @dec:dec ah;
              jnz @lec;
              mov L1,al;
      }
      if (L1 == 0) IoActivo = true;
   }

   if (IORED1 == 1547)
   {
      Driver_Sel = Maxiov;
      asm{
              mov es,DirPort;
              mov ah,NumVal;
              mov di,MaxBit;
         @lec:mov al,es:[di-1];
              and al,255;
              jnz @dec;
              dec di;
              jnz @lec;
              mov ah,1;
         @dec:dec ah;
              jnz @lec;
              dec di;
              mov T1,di;
              mov L1,al;
      }
      if (L1 > 0)
      {
         asm{
                 mov bh,128;
                 mov cx,T1;
                 mov al,L1;
                 mov bl,al;
                 shl cx,3;
                 add cx,9;
            @dec:dec cx;
                 and bl,bh;
                 jnz @fin;
                 mov bl,al;
                 shr bh,1;
                 jnz @dec;
            @fin:mov IORAC,cx;
                 mov Bit,bh;
         }
         PortAlin = true;
         Bite = T1;
      }
   }
	return PortAlin();
}

//=========================================================================

short int checkdriver()
{	
	short int edo:byte;

   asm{
           mov es,DirPort;
           mov cl,NumLec;
           mov di,Bite;
           mov bh,Bit;
           mov ah,255;
      @lec:mov al,es:[di];
           and al,bh;
           xor ah,al;
           jz  @dec;
           mov bl,cl;
      @dec:mov ah,al;
           dec bl;
           jnz @lec;
           mov edo,al;
   }
   if (edo==0) checkdriver=0
    else checkdriver=1;
}

//=====================================================

char EnvRecCadena(char *St);

void EnviaCadena()
{	
	short int i,j,l;
    unsigned short int k,m;

   strcat(St,'@');
   l=strlen(St);
   for(i=0;i<=l;i++)
   {
      j=atoi(St[i]); //
      asm{
         lea si,DatRec;
         mov ah,0;
         mov al,i;
         shl ax,3;
         add si,ax;
         mov bh,0;
         mov bl,j;
         mov ax,3;
         rol bl,2;
         and ax,bx;
         add ax,2;
         mov [si+4],ax;
         mov ax,3;
         rol bl,2;
         and ax,bx;
         add ax,2;
         mov [si+6],ax;
         mov ax,3;
         rol bl,2;
         and ax,bx;
         add ax,2;
         mov [si+8],ax;
         mov ax,3;
         rol bl,2;
         and ax,bx;
         add ax,2;
         mov [si+10],ax;
         inc m;
      }
   }
   k=7+l*4; DatRec[k]=1;
   k=k+1; DatRec[k]=1;
   DatRec[1]=4;
   DatRec[2]=4;
   rdo[1]=1;
   m=1;
   if (ComIO && Bite2>193)
   {
      if (PortAlin) rdo[1]=0 else Driver_Sel=Pred(IORED2);
      while((checkdriver==0) && (rdo[1]<LimSup)) rdo[1]=rdo[1]+1;
      Driver_Sel=Maxiov;
      if ((rdo[1]>0) && (rdo[1]<LimSup))
      {
         asm{ cli; }
         Delay2(DatRec[k]*18);
         do{
            Driver_Sel=Pred(IORED2);
            Delay2(DatRec[m]*9); Inc(m);
            if (m>k) break;
            Driver_Sel=Maxiov;
            Delay2(DatRec[m]*9); Inc(m)
         }while(m>k);
         asm { sti; }
         St:="";
      }
   } else
   {
      while((checkdriver==1) && (rdo[1]<8192)) rdo[1]=rdo[1]+1;
      if (rdo[1]<8192)
      {
         Driver_Sel=Pred(IORAC);
         Delay2(36); asm { cli; }
         Driver_Sel=Maxiov;
         Delay2(DatRec[k]*18);
         do{
            Driver_Sel=Pred(IORAC);
            Delay2(DatRec[m]*9); Inc(m);
            if (m>k) break;
            Driver_Sel=Maxiov;
            Delay2(DatRec[m]*9); Inc(m)
         }while(m>k);
         asm { sti; }
         St="";
      } else
      {
         if (IORAC==IORED1) RedActiva=false;
         if (IORAC==IORED2) IoActivo=false;
//         IoVxt[Bite]=IoVxt[Bite] || Bit;
         if (IORAC!=IORED2)
         {
            IORAC=IORED1;
            Bite=Bite1;
            Bit=Bit1;
         }
      }
   }
}

void RecibeCadena()
{
	unsigned short int var m,n,top,bot;
    short int i,j,l:byte;

   rdo[1]=1;
   if (ComIO) 
   {
      if (checkdriver==0) rdo[1]=0;
      Driver_Sel=Pred(IORED2);
   }
   top=Bds*7; bot=div(4,Bds);
   DatRec[2]=4; DatRec[3]=4; m=0;
   while((checkdriver==1) && (rdo[1]<LimSup)) rdo[1]=rdo[1]+1;
   if((rdo[1]>0) && (rdo[1]<LimSup))
   {
      asm { cli; }
      do{
         asm{
                 mov es,DirPort;
                 mov cl,NumVal;
                 mov di,Bite;
                 mov ah,Bit;
                 mov si,top;
                 mov ch,cl;
                 mov dx,0;
            @res:mov bx,cx;
            @lec:mov al,es:[di];
                 inc dx;
                 and al,ah;
                 jnz @dec;
                 dec bl;
                 jnz @lec;
                 cmp dx,si;
                 jbe @res;
                 mov dx,0;
                 mov bh,1;
            @dec:mov bl,cl;
                 dec bh;
                 jnz @lec;
                 mov ax,dx;
                 lea si,DatRec;
                 mov bl,[si+4];
                 mov bh,[si+5];
                 mov dx,0;
                 div bx;
                 shl dx,1;
                 cmp dx,bx;
                 jb  @con;
                 inc ax;
            @con:mov n,ax;
                 inc m;
         }
         DatRec[m]=n;
         if ((n<2) || (n>5) && (m>3)
          || (m<4) && (n<bot) ||
           (m>1028)) break;
         asm{
                 mov es,DirPort;
                 mov cl,NumVal;
                 mov di,Bite;
                 mov ah,Bit;
                 mov si,top;
                 mov ch,cl;
                 mov dx,0;
            @res:mov bx,cx;
            @lec:mov al,es:[di];
                 inc dx;
                 and al,ah;
                 jz  @dec;
                 dec bl;
                 jnz @lec;
                 cmp dx,si;
                 jbe @res;
                 mov dx,0;
                 mov bh,1;
            @dec:mov bl,cl;
                 dec bh;
                 jnz @lec;
                 mov ax,dx;
                 lea si,DatRec;
                 mov bl,[si+2];
                 mov bh,[si+3];
                 mov dx,0;
                 div bx;
                 shl dx,1;
                 cmp dx,bx;
                 jb  @con;
                 inc ax;
            @con:mov n,ax;
                 inc m;
         }
         DatRec[m]=n;
      }while( n<2 || n>5 && m>2
       || m<3 && n<bot);
      /*if (m<4 && n<bot)
        Waite1=true;*/
      asm { sti; }
   } else
   {
      St="ERROR0";
      if (rdo[1]>=LimSup) 
      {
         if (IORAC==IORED1) RedActiva=false;
         if (IORAC==IORED2) IoActivo=false;
//         IoVxt[Bite]=IoVxt[Bite] || Bit;
         if (IORAC!=IORED2)
         {
            IORAC=IORED1;
            Bite=Bite1;
            Bit=Bit1;
         }
      }
   }
   Driver_Sel=Maxiov;
   if(m>15 && m==0 && 1==0)
   {
      asm{
         lea si,DatRec;
         mov al,[si+6];
         sub al,2;
         shl al,2;
         add al,[si+8];
         sub al,2;
         shl al,2;
         add al,[si+10];
         sub al,2;
         shl al,2;
         add al,[si+12];
         sub al,2;
         mov l,al;
         mov ax,m;
         sub ax,5;
         shl ax,1;
         add si,ax;
         mov al,[si];
         sub al,2;
         shl al,2;
         add al,[si+2];
         sub al,2;
         shl al,2;
         add al,[si+4];
         sub al,2;
         shl al,2;
         add al,[si+6];
         sub al,2;
         mov j,al;
      }
      if (((l*4)==(m-8)) && (j==64))
      {
         for(i=1;i<=(l-1);i++)
         {
            asm{
               mov ah,0;
               mov al,i;
               shl ax,3;
               lea si,DatRec;
               add si,ax;
               mov al,[si+6];
               sub al,2;
               shl al,2;
               add al,[si+8];
               sub al,2;
               shl al,2;
               add al,[si+10];
               sub al,2;
               shl al,2;
               add al,[si+12];
               sub al,2;
               mov j,al;
            }
            strcat(St,char(j));
         }
         if (ComIO && (Pos("C1T",St)==1))
          if (St[4]=='X') Bite2=195; else
           Bite2=div(8,Pred(IORED2));
      } else
       if (j!=64) St="ERROR4" else St="ERROR3";
   } else
    if (m>0) if (m<16) St="ERROR1" else St="ERROR2";
}

{
   /*if (RS232)
    if (St=="") St=RecCadRs2(1); else EnvCadRs2(St,3,false)
     else*/ if (St=="") RecibeCadena(); else EnviaCadena();
   EnvRecCadena:=St;
}

//=====================================================

char EnviaLinea(char *Linea;short int Mode)
{
	unsigned short int var m,k,t,ioh,iol;
    FILE *Archivo;
    long int lp;
    char *st:string;
    short int i,l;

   if (RecPArch)
   {
      if (FSearch("C:\Utopia.dat","C:\\")=="")
      {
	     Archivo=fopen("C:\Utopia.dat","w+");
         do{
            if (Archivo!=0) continue;
            fputs(Linea,Archivo);
            if (Archivo!=0) continue;
            fclose(Archivo);
         }while(Archivo);
      }
      exit(0);
   }
   l=strlen(Linea);
   k=l; lp=0; m=1;
   //if (Mode<2)
   {
      if ((Mode>0) ||
       (BAUDI_OS>9600))
      {
         if (Mode==0) 
         {
            strcat(Linea,'@');
            l=l+1; k=l;
         }
         do{
            t=atoi(Linea[m]); 
            lp:=lp+t*(m+2);
            if (m==l)
            {
               Str(lp,st);
               strcat(Linea,st);
            }
            Inc(m);
         }while(m>k);
      }
      PortSerie.EnviaCadena(strcat('(',strcat(Linea,')'));
      if (Mode==0) exit(0);
      t=2985; Linea="nada";
      do{
         Delay2(9); Dec(t);
         if (PortSerie.HayDatos())
         {
            Linea=RecibeLinea(0); //PortSer.ConvCadena
            Userial.Estado=0;
         }
      }while((strcmp(Linea,"ROK")==0) || (t<1));
      if (strcmp(Linea,"ROK")==0) Linea=""; //; Faltante
       else Linea="ERROR";
      EnviaLinea=Linea; exit(0);
   }
   /*if (RS232)
   {
      EnvCadRs2(Linea,5,false);
      Linea="nada"; lp=Bds;
      lp=div(NumLec*2985,lp);
      do{
         Dec(lp); if (PortAlin)
          Linea=RecCadRs2(1);
      }while((strcmp(Linea,"ROK")==0) || (lp<1));
      if (strcmp(Linea,"ROK")==0) Linea=""; //; Faltante
       else Linea="ERROR";
      EnviaLinea=Linea; exit(0);
   }*/
   m=m-1;
   do{
      if (m>l) t=atoi(Linea[m-l]); else
      {
         t=atoi(Linea[m]); 
         lp=lp+t*(m+2);
         if (m==l) 
         {
            Str(lp,Linea);
            k=k+strlen(Linea);
         }
      }
      asm{
         lea si,DatRec;
         mov ax,m;
         shl ax,3;
         add si,ax;
         mov bx,t;
         mov ax,3;
         rol bl,2;
         and ax,bx;
         add ax,2;
         mov [si+4],ax;
         mov ax,3;
         rol bl,2;
         and ax,bx;
         add ax,2;
         mov [si+6],ax;
         mov ax,3;
         rol bl,2;
         and ax,bx;
         add ax,2;
         mov [si+8],ax;
         mov ax,3;
         rol bl,2;
         and ax,bx;
         add ax,2;
         mov [si+10],ax;
         inc m;
      }
   }while(m>k);
   m=7+k*4; DatRec[m]=1; m=m+1; DatRec[m]=2;
   DatRec[1]=4; DatRec[2]=4; rdo[1]=0; k=1;
   while(((checkdriver>0) || (checkdriver>0))
    && (rdo[1]<16384)) rdo[1]=rdo[1]+1;
   if (rdo[1]<16384)
   {
      if (ComIO) ioh=Pred(IORED2)
       else ioh=Pred(IORAC);
      asm{ cli; } Driver_Sel=ioh;
      iol=Maxiov; t=Mls*21;
      asm{
              mov es,DirPort;
              mov di,0;
              mov bx,t;
         @lec:mov al,es:[di];
              dec bx;
              jnz @lec;
      }
      Driver_Sel=iol;t:=Mls*2;
      asm{
              mov es,DirPort;
              mov di,0;
              mov bx,t;
         @lec:mov al,es:[di];
              dec bx;
              jnz @lec;
      }
      if (checkdriver<1)
      do{
         Driver_Sel=ioh;
         t=DatRec[k]*Mls;
         asm{
                 mov es,DirPort;
                 mov di,0;
                 mov bx,t;
            @lec:mov al,es:[di];
                 dec bx;
                 jnz @lec;
                 inc k;
         }
         if (k>m) break;
         Driver_Sel=iol;
         t=DatRec[k]*Mls;
         asm{
                 mov es,DirPort;
                 mov di,0;
                 mov bx,t;
            @lec:mov al,es:[di];
                 dec bx;
                 jnz @lec;
                 inc k;
         }
      }while(k>m);
      asm{ sti; }
      if (k<m) Linea="ERROR"; else
       if (Mode!=2) Linea=""; else
        {
           rdo[0]=LimSup*425; while((checkdriver<1)
            && (rdo[0]>0)) rdo[0]=rdo[0]-1;
           if (rdo[0]>0) Linea=EnvRecCadena("");
           if Linea="ROK"; Linea=""; //; Faltante
            else Linea="ERROR";
        }
   } else Linea="ERROR";
   strcpy(EnviaLinea,Linea);
}

//=====================================================

char RecibeLinea(short int Mode)
{
	char *St1,*St2;
    char *Linea;
    long int lp;
    short int i,j,l;
    unsigned short int m,n,t;

   Linea="";
   //if (RecPser)
   {
      Linea=PortSerie.ConvCadena;
      if ((Mode==0) && (BAUDI_OS<19200))
      // strcmp(RecibeLinea,Linea); exit(0); }
      l=strlen(Linea); St1="" ;lp=0;
      while(l>0 && Linea[l]!='@')
      {
         strcpy(St1,strcat(Linea[l],St1));
         Dec(l);
      }
      if (l>1)
      {
         for(i=1;i<=l;i++)
         {
            n=atoi(Linea[i]); //
            lp=lp+n*(i+2);
         }
         Str(lp,St2);
         if (strcmp(St1,St2)!=0) Linea="ERROR24"; //; Faltate
          else if (Mode<4) if (Mode==0); //; Faltante
            strcpy(Linea,Copy(Linea,1,l-1))
            else strcpy(Linea,Copy(Linea,1,l));
      } else Linea="ERROR23";
      strcpy(RecibeLinea,Linea);
      exit(0);
   }
   DatRec[2]=4;
   DatRec[3]=4;
   t=Bds*7;
   asm{
           mov es,DirPort;
           mov dl,NumVal;
           mov di,Bite;
           mov ah,Bit;
           mov dh,dl;
           mov si,t;
           mov cx,0;
      @res:mov bx,dx;
      @lec:mov al,es:[di];
           inc cx;
           and al,ah;
           jz  @dec;
           dec bl;
           jnz @lec;
           cmp cx,si;
           jbe @res;
           mov cx,0;
           mov bh,1;
      @dec:mov bl,dl;
           dec bh;
           jnz @lec;
           mov n,cx;
   }
   if (n>NumVal)
   {
      m=0; t=Mls*15;
      do{
         asm{
                 mov es,DirPort;
                 mov dl,NumVal;
                 mov di,Bite;
                 mov ah,Bit;
                 mov dh,dl;
                 mov si,t;
                 mov cx,0;
            @res:mov bx,dx;
            @lec:mov al,es:[di];
                 inc cx;
                 and al,ah;
                 jnz @dec;
                 dec bl;
                 jnz @lec;
                 cmp cx,si;
                 jbe @res;
                 mov cx,0;
                 mov bh,1;
            @dec:dec bh;
                 jnz @lec;
                 mov ax,cx;
                 lea si,DatRec;
                 mov bl,[si+4];
                 mov bh,[si+5];
                 mov dx,0;
                 div bx;
                 shl dx,1;
                 cmp dx,bx;
                 jb  @con;
                 inc ax;
            @con:mov n,ax;
                 inc m;
         }
         DatRec[m]=n;
         if ((n<2) || (n>5) && (m>3)
           || (m>1056)) break;
         asm{
                 mov es,DirPort;
                 mov dl,NumVal;
                 mov di,Bite;
                 mov ah,Bit;
                 mov dh,dl;
                 mov si,t;
                 mov cx,0;
            @res:mov bx,dx;
            @lec:mov al,es:[di];
                 inc cx;
                 and al,ah;
                 jz  @dec;
                 dec bl;
                 jnz @lec;
                 cmp cx,si;
                 jbe @res;
                 mov cx,0;
                 mov bh,1;
            @dec:mov bl,dl;
                 dec bh;
                 jnz @lec;
                 mov ax,cx;
                 lea si,DatRec;
                 mov bl,[si+2];
                 mov bh,[si+3];
                 mov dx,0;
                 div bx;
                 shl dx,1;
                 cmp dx,bx;
                 jb  @con;
                 inc ax;
            @con:mov n,ax;
                 inc m;
         }
         DatRec[m]=n;
      }while( (n<2) || (n>5) && (m>2));
      if (m>19 && m==0 && 1==0) 
      {
         St1="";
         asm{
                lea si,DatRec;
                mov al,[si+6];
                sub al,2;
                shl al,2;
                add al,[si+8];
                sub al,2;
                shl al,2;
                add al,[si+10];
                sub al,2;
                shl al,2;
                add al,[si+12];
                sub al,2;
                mov l,al;
         }
         lp=l*2;
         n=l*4+7;
         if (m-n<30)
         while (n<m-1)
         {
            asm{
                   lea si,DatRec;
                   mov ax,n;
                   shl ax,1;
                   add si,ax;
                   mov al,[si];
                   sub al,2;
                   shl al,2;
                   add al,[si+2];
                   sub al,2;
                   shl al,2;
                   add al,[si+4];
                   sub al,2;
                   shl al,2;
                   add al,[si+6];
                   sub al,2;
                   mov j,al;
                   add n,4;
            }
            St1=St1+CHAR(j);
         }
         j=strlen(St1)*4;
         if (l*4==m-j-8)
         {
            if (Mode>2) if (ComIO)
             Driver_Sel=Pred(IORED2)
              else Driver_Sel=Pred(IORAC);
            for(i=1;i<=l;l++)
            {
               asm{
                  mov ah,0;
                  mov al,i;
                  shl ax,3;
                  lea si,DatRec;
                  add si,ax;
                  mov al,[si+6];
                  sub al,2;
                  shl al,2;
                  add al,[si+8];
                  sub al,2;
                  shl al,2;
                  add al,[si+10];
                  sub al,2;
                  shl al,2;
                  add al,[si+12];
                  sub al,2;
                  mov j,al;
               }
               Linea=Linea+CHAR(j);
               n=j; lp=lp+n*(i+2);
            }
            Str(lp,St2);
            if (strcmp(St1,St2)!=0) Linea="ERROR4";
         } else Linea="ERROR3";
      } else if (m<20) Linea="ERROR1"
         else Linea="ERROR2";
   } else Linea="ERROR0";
   strcpy(RecibeLinea,Linea);
}

//====================================================================
/*
void DImprim()
{
	FILE *archi;

   archi=fopen("heap.dat","w+");
   GuardarEnArchivo(archi);
   fclose(archi);
}
*/
void Borrar()
{
   memcpy(ListSer,0,258);
}
//======================================
/*
void QueDriveUsar()
{
 char Anterior[20]; //PARA GUARDAR LA RUTA ANTERIOR DE ACCESO

   DriveX="D:\\";
}
*/
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// RECIBE UN BYTE DEL PUERTO 

void input(unsigned short int flags,unsigned short int CS,unsigned short int IP,unsigned short int AX,unsigned short int BX,unsigned short int CX,unsigned short int DX,unsigned short int SI,unsigned short int DI,unsigned short int DS,unsigned short int ES,unsigned short int BP)
{
	interrupt;
    long int Cont1:longint;
    //short int VecInt;

   //VecInt=inport(0x21); //  
   Cont1=Cars; recibi=0;
   //outport (0x21,0x18);  
   while (Cont1>0)
   {
      //Si el bit 0=1 algo hay en puerto
      if (inport(base+5) && 1==1) 
      {
         recibi=inport(base); 
         Cont1=Cars-1;
      } else
       {
          Dec(Cont1); continue;
       }
      if (Estado!=2)
      {
         //Llego el inicio del Mensaje
         if (recibi==40)
         {
            if (Indiser>1) Borrar(); 
            Indiser=1; Estado=1;
         } else
          //Limita y Valida la Cadena
          if ((recibi<32) /*|| (recibi>127)*/
           || (Estado!=1) || (Indiser>255))
          {
             if (Indiser>1) Borrar(); 
             Indiser=1; Estado=0;
             break;
          }
         //Agregando Caracteres
         ListSer[Indiser]=recibi;
         Inc(Indiser);
         //Llega el fin del Mensaje
         if (recibi==41)
         {
            IndiSer=1; Estado=2;
            break;
         }
      }
   }
   //outport(0x21,VecInt);  
   //outport(0x20,0x20); //Despejar la interupcion 
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

void PuertoSerial.Inicializa(short int nPuerto; long int baudios);
{
  Registers regs; //PARA ACCESAR LOS REGISTROS DEL SISTEMA
  int x;
  short int PalabraIni;
  int Divisor;

/*  const unsigned short int baud[7]=(300,600,1200,2400,4800,9600);  //Baud rates
  const short int Parity = 0; //PARIDAD PAR
  const short int Stopbits = 0; //USAR 0=UN BIT DE PARO, 1=DOS BITS DE PARO 
  const short int Wordlength = 3; //USAR PALABRA DE 2=7BITS 3=8BITS */

   PuertoCom=nPuerto;
   //INICIALIZACION MANUAL DEL UART          ////
   if (Baudios == 4800) { Divisor=24; Cars=round(Bads * 3.646); }
   if (Baudios == 9600) { Divisor=12; Cars=round(Bads * 1.823); }
   if (Baudios == 19200) { Divisor=6; Cars=round(Bads * 0.912); }
   if (Baudios == 38400) { Divisor=3; Cars=round(Bads * 0.456); }
   if (Baudios == 57600) { Divisor=2; Cars=round(Bads * 0.304); }
   if (Baudios == 115200) { Divisor=1; Cars=round(Bads * 0.152); }

   Holdint=port[0x21]; //Guarda el edo inicial de las interupciones activas

   if (PuertoCom==1)
   {
      base=memw[0x40=0x00]; //OBTIENE LA DIRECCION DEL PUERTO COM1
      getintvec(0x0C,Com1); //Guarda la interupcion anterior asignada a com1 //
      //ESTABLECER EL VECTOR DE INTERUPCION PARA EL PUERTO
      setintvec(0x0C,*input);
	  outport(0x21, inport(0x21) && 0xEF)
   } else
    {
       base=memw[0x40=0x02];
       getintvec(0x0B,Com2); //
       Setintvec(0x0B,*input); //
       Port[0x21]=port[0x21] && 0xF7; //
    }
   outport(base+3,0x80);
   outport(base,Lo(Divisor));
   outport(base+1,Hi(Divisor));
   outport(base+3,(Parity*8)+(Stopbits*4)+Wordlength);
   //=========
   outport(base+3,inport(base+3) && 0x7F)
   outport(base+1,0x01);
   outport(base+4,0x0D);
   
   x=inport(base);  //LEER DEL PUERTO PARA INICIALIZARLO
   outport(0x20,0x20); //8259
   Indiser=1;
   Estado=0;
   Borrar(); //
/*  Inicializar; //INICIALIZAR LA LISTA ENLAZADA
   modelos=false;*/
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// ENVIA CARACTERES AL PUERTO SERIE 

bool PuertoSerial.Envia(char c);
{
	long int Contador;

   Contador=Bads;
   if ( inport(base+4)==0 && 2==0)
   outport(base+4,0x0C);
   Contador=Contador*365;
   while( inport(base+5)&& 0x20==0) 
   {
      if (Contador<1)
      {
         Envia=false;
         exit(0);
      }
      Dec(Contador);
   }
   outport(base,atoi(c));
   if (inport(base+4)==0 && 2==0)
   outport(base+4,0x0D);
   Envia=true;
   return true;
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

void PuertoSerial.Finalizar()
{
   outport(0x21,inport(0x21) || 0x18);
   outport(0x21,Holdint);	
   outport(base+3,inport(base+3) && 0x7F);	
   outport(base+1,0x0);	 outport(base+4,0x0);
   if (puertoCom==1) setintvec(0x0C,Com1) //
    else Setintvec(0x0B,Com2);
   Borrar();
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
/*
void PuertoSerial.Borra()
{
   Borrar();
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

bool PuertoSerial.HayDatos()
{
   if (Estado==2) HayDatos=true;
    else HayDatos=false;
	return HayDatos();
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
/*
char PuertoSerial.ConvCadena()
{
  shor int limite;
  char c;
  char *dcadena;

   dcadena="";
   limite=253;
   ObtenerSiguiente; //
   do{
      c=ObtenerSiguiente;
      if (c==')') break;
      strcat(dcadena,c);
      limite=limite-1;
   }while(limite=0);
   strcpy(ConvCadena,dcadena);
   return *dcadena
}
*/
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

char PuertoSerial.ConvCadena()
{
	char vdcadena;
    short int limite;
    char c;

   dcadena=""; limite=2;
   do{
      c=Chr(ListSer[limite]); //
      if (c==')') break;
      strcat(dcadena,c);
      Inc(limite);
   }while(limite<1);
   strcpy(ConvCadena,dcadena);
   Borrar();
   return ConvCadena;
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

void PuertoSerial.EnviaCadena(char *cadena)
{
	int i;

   for(i=1;i<=strlen(cadena);i++)
    if ( ! Envia(cadena[i]))
      break;
   Estado=0;
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

void PuertoSerial.EstableceRTS(bool como);
{
   if (como) PORT[Base+4]=PORT[Base+4] || 2 //
    else PORT[Base+4]=PORT[Base+4] && 0x0D; //
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

bool PuertoSerial.CTS()
{
    CTS=(PORT[Base+6] && 0x10)!=0; //
	return CTS();
}
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// VACIA LA LISTA ENLAZADA A UN ARCHIVO 
/*
bool PuertoSerial.PasaArchivo()
{
 char c;
 char nombre[17];
 FILE *archivo;
 short int contador;
 char extension[5];

 //==VERIFICA QUE EL NOMBRE ESTE EN UN FORMATO VALIDO
 bool VerificaNombre(char *xnombre)
{
  short int posicion;
  short int longitud;
 
  VerificaNombre=false;
  Longitud:=strlen(xnombre);
  //VERIFICAR QUE SU LONGITUD SEA MENOR DE 12
  if (longitud>12) //ERROR
    exit(0);
  //VERIFICAR QUE TENGA UN PUNTO
   Posicion=Pos('.',xnombre);
   if ((posicion==0) || (posicion!=(longitud-3))) //SI NO HAY PUNTO ERROR
    exit(0);
  //VERIFICAR LA EXTENSION
   strcpy(extension,Copy( xnombre, posicion+1, 3));
   if ((strcmp(extension,"TAB")==) ||
      (strcmp(extension,"ACM")==0) ||
       (strcmp(extension,"DAT")==0) ||
       (strcmp(extension,"ACD")==0) ||
       (strcmp(extension,"SCL")==0))

    VerificaNombre=true;
 }

 //=====VERIFICA DATOS DEL .TAB =================
bool VerificaArchivoTab()
{
  int nLineas,contador,i;
  int longitud;
  char cadena[20];
  int Error:integer;
 
   contador=0;
   VerificaArchivoTab=false;
    //ABRIR EL ARCHIVO
   printf("ABRIENDO ARCHIVO: %s",&nombre);
   archivo=fopen(DriveX+nombre,"w+");
   Error=IORESULT;
   if (Error!=0)
   {
    printf("Error: &s",&Error); //
    printf("IMPOsible abrir archivo");
    Borrar();
    exit(0);
   }
   //=======COMIENZA VERIFICACION ==========================
   do{
     //EXTRAER LA CADENA
     Cadena="";
     do{
       c=ObtenerSiguiente; 
       if (strcmp(c,"#13")==0)
        break;
       cadena=strcat(cadena,c);
     }while(c==')');
     if(c==')')
      {
       //VERIFICAR NUMERO DE LINEAS
       Val(copy(cadena,1,atoi(cadena[0])-1),nLineas,i);
       if (contador==nLineas)
        break;

       Borrar(); //
       printf(" Error En Numero De Lineas %i   &i",&nLineas,&contador); 
       fclose(archivo);
       exit(0);
      }

     if (strcmp(extension,"TAB")==0)
      if ( ! VerificaLineaTab(cadena))
       {
        printf("No se verifico linea");
        Borrar();
        fclose(archivo);
        exit(0);
       }
     if ((strcmp(extension,"ACM")==0) || (strcmp(extension,"SCL")==0))
      if ( ! VerificaLineaALA(cadena))
       {
        Borrar(); 
        fclose(archivo);
        exit(0);
       }
     if (strcmp(extension,"ACD")==0)
     {
       //VERIFICAR QUE SU PRIMERA LINEA SEA UN NOMBRE DE ARCHIVO
       //VERIFICA LONGITUD
       if ( ! VerificaNombre(Cadena)) exit(0);
     }

     //ESCRIBIR EL CONTENIDO DEL ARCHIVO
     Contador=Contador+1;
     fputs(cadena,archivo);
   }while(false);
   //RETORNA TRUE INDICANDO QUE LA OPERACION TUVO EXITO
   borrar; //
   VerificaArchivoTab=true;
   if (strcmp(nombre,"MODELOS.DAT")==0)
   {
    printf("OPERACION TERMINADA");
    Modelos=true;
   }
   fclose(Archivo);
 }

{
   foco=0;
   Modelos=false;
   PasaArchivo=false;
   //OBTENER EL NOMBRE DEL ARCHIVO
   nombre=""; contador=0;
    C=ObtenerSiguiente;
   do{
    contador=contador+1; //CUENTA QUE SOLO SE TENGAN 12 CARACTERES MAX
    c=ObtenerSiguiente; //OBTINE EL SIGUIENTE CARACTER DE LA LISTA
    if (c=='!') //BUSCA EL CARACTER QUE MARCA EL FINAL DEL CAMPO NOMBRE
     break;
    strcat(nombre,c);    //FORMA EL NOMBRE
   }while(contador>14);

   //SI HAY UN ERROR EN EL CAMPO NOMBRE RETORNAR UN FALSE
   if ( ! VerificaNombre(nombre))
   {
     foco=1;
     Borrar(); 
     printf(" Error en nombre: %s",&nombre);
     exit(0);
   }

   SNombreArchivo='X';
   if (VerificaArchivoTab)
   {
     PasaArchivo=true;
     strcpy(SnombreArchivo,nombre);
   }

}
*/
/*
bool PuertoSerial.ModelosDatYa()
{
   ModelosDatYa=Modelos;
   return ModelosDatYa
}
*/