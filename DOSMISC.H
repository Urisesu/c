/***** RUTINAS DEL DOS     *********/

//COMPILER
#include<dos.h>
#include<string.h>
#include<dir.h>
#include<stdio.h>
#include<stdlib.h>

//SYSTEM
#include "Decompre.h"
#include "Global.h"
#include "graphics.h"
#include "functions.h"

std::vector::swap;

typedef String6 string6[6];
typedef String50 string50[50];
//MiProc&  = void(char s[80]);
typedef char* const 	Openstring; // D16 string/D32 

bool DosFamilias;
//MiFunc : Miproc; Pendiente

char ObtenerTokenAt(char *cadena,int index, char separador);
int StrToInt(char *cadena);
char *str2(int numero);
char *IntToStr(int valor);

int BorraDir(char *nombre); //BORRA LOS ARCHIVOS DE UN DIRECTORIO
void CreaDirSinoExiste(char *nombre);
 /*void RenombraD0a1a2();
  void RenombraD2a1a0();
  void DescomprimeALA();
  void EstableceCamino();
  void BorraTablas2();
  void MoverAC();
  void MoverAD();*/
bool CopiaArchivosDeModelos(String6 Drivem, OpenString mensaje);
void BorraTablasDe(char *Directorio);
//void AcoplaArchivos();
bool CopiaArchivoDeX(char *nombre,char *fuente,char *destino, OpenString mensaje);

typedef String12 = String[12];

const int _Continuar = -10;

/*============================================================================*/

char ObtenerTokenAt(char *cadena,int index, char separador){
  int  i, longitud, contador;
  char tempo[50];
  contador = 1;
  strcpy(tempo,"");
  longitud = strlen(cadena);

  for(i = 0; i < longitud; i++){
    if((i < longitud) && (cadena[i] == separador) && (cadena[i+1] != separador)){
      contador++;
    }    
    if(cadena[i] == separador)
      continue;
    if(contador > index)
      break;
    if(contador == index)
      strcat(tempo,cadena[i]);
  }
  return tempo;
}

int StrToInt(char *){
  int i , codigo;
  i = atoi(cadena);
  if(i == 0){
    return 0;
  }
  else{
    return i;
  }
}

char *str2(int numero){
  char *tempo, *tempo2;
  tempo = itoa(numero);
  if(strlen(tempo) == 1){
    tempo2 = '0';
    strcat(tempo2,tempo);
    strcpy(tempo,tempo2);    
  }
  return tempo;
}

char *IntToStr(int valor){
  char *cadena;
  cadena = itoa(valor);
  return cadena;
}

//=====================================================

//============================================================================

/*void EstableceCamino()
{
  char Anterior[10];

  getdir(3,anterior); 
  chdir("D:\\");
  if (IORESULT==0)
  //strcpy(camino,"D:\\"); 
  else
   strcpy(camino,anterior);
  if (camino[ord(camino[0])]!='\\')
   strcat(Camino,'\\');

  {chdir(anterior);
//};

void DescomprimeALA()
{
  TSearchRec DirInfo;
  FILE *F; 
  int i;
  char *temporal;
  Findfirst(strcat(camino,"*.ACM"), Archive, DirInfo); 
  while(DosError == 0)
  {
    temporal="";
    for(i=1;i<=strlen(DirInfo.Name);i++) 
    {
      if (DirInfo.Name[i]='.') 
       break;
	   strcat(temporal,DirInfo.Name[i]);
    }
    DesComprime(strcat(camino,strcat(DirInfo.name,strcat(camino,strcat(Temporal,".ala")))));
	//BORRAR EL ARCHIVO COMPRIMIDO 
    remove(DirInfo.name);
    findnext(DirInfo); 
  }
}
*/

//============================================================================//

int BorraDir(char *nombre){
  TSearchRec DirInfo;
  char *Direct;
  int Lnm;
  File *F;

  Lnm = strlen(nombre); Direct = nombre;
  if(Pos('\\',Direct)  == 0)
    Direct = "";
  else{
    while(Direct[Lnm] != '\\'){
        Delete(Direct,Lnm,1);
        Lnm = strlen(Direct);
    }
  }
  Findfirst(nombre,AnyFile,DirInfo);
  while (DosError == 0) 
  {
	  remove(DirInfo.name);
  }
  return lnm;
}

//============================================================================
/*
int RenombraExt(char *extencion,char *extencion2);
{
  TSearchRec DirInfo;
  FILE *F; 
  temporal[12];
  int i;

  temporal="";
  findfirst(strcat(camino,strcat("*.",strcat(extencion,strcat(Archive,DirInfo)))));
  while(findfirst(strcat(camino,strcat("*.",strcat(extencion,strcat(Archive,DirInfo)))))==0)
  {
    F=fopen(DirInfo.Name,"r"); 
    //OBTIENE EL NOMBRE DEL ARCHIVO SIN LA EXTENCION
    temporal="";
    for(i=1;i<=Length(DirInfo.Name);i++)
    {
      if (DirInfo.Name[i]='.') break;
	  strcat(temporal,DirInfo.Name[i])
    }
    //RENOMBRA EL ARCHIVO
	rename(DirInfo.name, strcat(camino,strcat(temporal,strcat('.',extencion2))));
    findnext(DirInfo); 
  }
}
*/
//============================================================================//

void CreaDirSinoExiste(char *nombre)
{
  TSearchRec DirInfo;
  findfirst(nombre,Directory,DirInfo); 
  if (errno!=0) mkdir(nombre);

//============================================================================

void BorraTablasDe(char *Directorio:string)
{
   BorraDir(strcat(Directorio,"*.tab")); 
   BorraDir(strcat(Directorio,"*.ala"));
   BorraDir(strcat(Directorio,"*.scl"));
}

//==================================================================
/*
void RenombraD0a1a2()
{
   BorraDir(strcat(camino,"*.ta2"));
   BorraDir(strcat(camino,"*.al2"));
   BorraDir(strcat(camino,"*.da2"));
   BorraDir(stcat(camino,"*.sc2"));
   //ARCHIVOS ALA
   RenombraExt("AL1","AL2");
   RenombraExt("ALA","AL1");
   //ARCHIVOS TAB
   RenombraExt("TA1","TA2");
   RenombraExt("TAB',"TA1");
   //ARCHIVOS SCL
   RenombraExt("SC1","SC2");
   RenombraExt("SCL","SC1");
   //ARCHIVO DAT
   RenombraExt("DA1","DA2");
   RenombraExt("DAT","DA1");
}

//============================================================================

void RenombraD2a1a0()
{
   //BORRA ALA TAB SCL Y DAT
   BorraDir(strcat(camino,"*.ala"));
   BorraDir(strcat(camino,"*.tab"));
   BorraDir(strcat(camino,"*.scl"));
   BorraDir(strcat(camino,"*.dat"));
   //RENOMBRA A ALA TAB SCL DAT
   RenombraExt(strcat("AL1","ALA"));
   RenombraExt(strcat("AL2","AL1"));
   RenombraExt(strcat("TA1","TAB"));
   RenombraExt(strcat("TA2","TA1"));
   RenombraExt(strcat("SC1","SCL"));
   RenombraExt(strcat("SC2","SC1"));
   RenombraExt(strcat("DA1","DAT"));
   RenombraExt(strcat("DA2","DA1"));
}

//============================================================================

void BorraTablas2()
{
   BorraDir(strcat(camino,"*.ta2"));
   BorraDir(strcat(camino,"*.al2"));
   BorraDir(strcat(camino,"*.sc2"));
   BorraDir(strcat(camino,"*.da2"));
}
*/
//==================================================================

bool Exen(char *Comando)
{
   Exen=false; swap(); Exec(getenv("COMSPEC"),strcat("/C " 
    ,Comando)); system(getenv("COMSPEC"),"/C cls"); 
   swap(); if (errno==0) Exen=true; 
   if (UsaResetTecla)
   {
      setfillstyle(1,8); 
      bar(5,INICIO_POSY-50,GetMaxX-5,INICIO_POSY+200);
      setfillstyle(1,0);
      bar(12,INICIO_POSY-5,GetMaxX-10,INICIO_POSY+65);
      monitor.x=INICIO_POSX;
      monitor.y=INICIO_POSY;
      monitor.index=0;
      MainMenuBtn[1].cad="  S"; 
      MainMenuBtn[2].cad=" Izq";
      MainMenuBtn[3].cad=" Arr";
      MainMenuBtn[4].cad=" Der";
      MainMenuBtn[5].cad=" Rst";
      MainMenuBtn[1].Inicializa(290,220,35,34);
      MainMenuBtn[2].Inicializa(327,220,35,34);
      MainMenuBtn[3].Inicializa(364,220,35,34);
      MainMenuBtn[4].Inicializa(401,220,35,34);
      MainMenuBtn[5].Inicializa(438,220,35,34);
   }
   return true;
}

//==================================================================
/*
void MoverAC()
{
  char ruta[20]; 

  if (strcmp(camino,"D:"')!=0) exit(0); 
  getdir(3,ruta);
  //swap();
  //system("c:\Command.Com",strcat("/c Move D:*.tab ",ruta));
  //swap();
  //===
  system("Move.exe",strcat("D:*.tab ",ruta));
  //swap();
  //system("c:\Command.Com",strcat("/c Move D:*.ala ",ruta));
  //swap();
  //===
  system("Move.exe",strcat("D:*.ala ",ruta));
  //swap(); 
  //system("c:\move.exe",strcat("D:*.scl ",ruta)); 
  //swap();
  //===
  system("move.exe",strcat("D:*.scl ",ruta)); 
  //swap(); 
  //system("C:\Command.Com",strcat("/c Move D:modelos.dat "+ruta));
  //swap();
  exen("Move.exe",strcat("D:modelos.dat ",ruta)); 
}

//===================================================================

//MUEVE LAS TABLAS DE C: AL DISCO D:
void MoverAD()
{
  char ruta[20];

  if (strcmp(camino,"D:\")!=0) exit(0);
  getdir(3,ruta);
  if (ruta[ord(ruta[0])]!='\\')
   strcat(ruta,'\');
  //swap(); 
  //system("c:\Command.Com", strcat("/c Move ",strcat(Ruta,"*.tab D:")));
  //swap(); 
  //if (errno==8) exit(0);
  exen("Move.exe",strcat(ruta,"*.tab D:"));
  //swap(); 
  //system("c:\Command.Com", strcat("/c Move ",strcat(Ruta,"*.ala D:"))); 
  //swap(); 
  //swap();
  exen(strcat("Move.exe",strcat(ruta,"*.ala D:"))); 
  exen(strcat("Move.exe",strcat(ruta,"*.scl d:"))); 
  //swap(); 
  //system("C:\Command.Com", strcat("/c Move ",strcat(Ruta,"modelos.dat D:")));
  //swap(); 
  exen("Move.exe",strcat(ruta,"modelos.dat D:"));
}
*/
//===================================================================

bool CopiaArchivosDeModelos(char Drive[6],char *Mensaje)
{
  char *Ruta; 
  TSearchRec DirInfo;
  ModelArchi[12];
  int Contador,Error,i;
  return true; //Valor de retorno ooooo
}

 //=====LOCAL=====
 //===================================================================
 bool CopiaModelo(char *Modelo) 
{ 
	short int SiExiste;
    char *XDrive;

   SiXT=false; SiHD=false; CopiaModelo=true; SiExiste=0;
   findfirst(strcat(Drive,strcat(Modelo,".tab",Archive,DirInfo); 
   if (errno == 0) SiExiste=1; else 
    {
       findfirst(strcat(Drive,strcat("TABLAS\\",strcat(Modelo,".tab"))),Archive,DirInfo);
       if (errno == 0) SiExiste=2;
    }

   //Escribe_Al_Display("");

   if (SiExiste > 0)
   {
     XDrive=Drive;
     if (SiExiste == 2) strcpy(XDrive,strcat(Drive,"TABLAS\\"));
     //Escribe_Al_Display(strcat(Drive,strcat("TABLAS\\",strcat(Modelo,".TAB "))));
     if (! CopiaArchivoDeX(strcat(Modelo,".tab"),XDrive,Ruta,mensaje))
     {
       CopiaModelo=false; exit(0);
     }
     if (SiExiste == 2) strcmp(XDrive,strcat(Drive,"IOSALA\\"));
     //Escribe_Al_Display(strcat(Drive,strcat("IOSALA\\",strcat(Modelo,".ALA "))));
     if (! CopiaArchivoDeX(strcat(Modelo,strcat(".ala",strcat(XDrive,strcat(Ruta,mensaje))))))
     {
       CopiaModelo=false; exit(0); 
     }
     if ((FSearch(strcat(Drive,strcat("SECLIP\\",strcat(Modelo,".scl"))),strcat(Drive,"SECLIP\\")!="") //
      || (FSearch(Drive+Modelo+'.scl',Drive)!=''))
     {
       CreaDirSinoExiste("SECLIP");
       if (SiExiste == 2) stpcpy(XDrive,strcat(Drive,"SECLIP\\"));
       //Escribe_Al_Display(strcat(Drive,strcat("SECLIP\\",strcat(Modelo,".SCL "))));
       if (! CopiaArchivoDeX(strcat(Modelo,strcat(".scl",strcat(XDrive,strcat(Ruta,mensaje))))))
       {
         CopiaModelo=false; exit(0);
       }
     }
   } else 
    {
      Escribe_Al_Display(strcat("NO EXISTE EL ARCHIVO",Drive+DirInfo.Name));
      do{ }while(VerificaReset=_TRUE); CopiaModelo=false; //Define _TRUE
    }

   SiXT=true; SiHD=true;
   return SiXT;

 }

 //===================================================================

 //=====LOCAL=====
 void CargaModelosAVariable()
{
   FILE *Archivo;
   char *Linea;

   //ABRIR MODELOS .DAT DE LA UNIDAD A
   Archivo=fopen(strcat(Drive,"Modelos.dat"),"r");
   errno = IORESULT;
   if (errno!=0)
   {
	switch(errno)
	{
       case 151 : strcpy(Mensaje,strcat("UNIDAD ",strcat(Drive," NO CONFIGURADA")));
       case 152 : strcpy(Mensaje,strcat("Drive ",strcat(Drive," No listo")));
       case 157 : strcpy(Mensaje,strcat("No existe Unidad ",Drive));
       case 158 : strcpy(Mensaje,"Sector no encontrado");
       case 162 : strcpy(Mensaje,"Drive posiblemente Descompuesto");
       case 2 : strcpy(Mensaje,"Modelos.dat no presente");
       else strcpy(Mensaje,"  *Error*  ");
     }
     exit(0);
   }

   //BORRAR ARHIVOS ANTERIORES
   Escribe_Al_Display("Borrando Archivos    TABLAS\*.tab...   ");
   BorraDir("*.tab"); BorraDir(strcat(Ruta,"TABLAS\*.tab"));
   Escribe_Al_Display("Borrando Archivos    IOSALA\*.ala...   ");
   BorraDir("*.ala"); BorraDir(strcat(Ruta,"IOSALA\*.ala"));
   Escribe_Al_Display("Borrando Archivos    SECLIP\*.scl...   ");
   BorraDir("*.scl"); BorraDir(strcat(Ruta,"SECLIP\*.scl"));
   //CARGAR LOS MODELOS A LA VARIABLE
   Contador = _CONTINUAR; 
   DosFamilias = false;

   while(! feof(Archivo))
   {
     fgets(Linea,100,Archivo);
     if(Archivo)
     {
       strcpy(Mensaje,strcat("Error en unidad ",Drive));
       Error = IORESULT; exit(0); //Pendiente
     }

     //BUSCAR LA LINEA DE S's Y N's
     if (((linea[1] == 'S' || linea[1] == 's' || linea[1] == 'N' || linea[1] == 'n') ? true : false)
      && (linea[2] == 'S' || linea[2] == 's' || linea[2] == 'N' || linea[2] == 'n') ? true : false)
       && ((linea[8] == '1' || linea[8] == '2' || linea[8] == '3' || linea[8] == '4' || linea[8] == '5' || linea[8] == '6' || linea[8] == '7' || linea[8] == '8' || linea[8] == '9' ) ? true : false))
        && (Contador==_CONTINUAR))
     {
	   fgets(Linea,100,Archivo);//LEER LA FAMILIA 
       fgets(Linea,100,Archivo); //Pendiente
       //VERIFICAR SI LA LINEA LEIDA ES FAMILIA O MEDELO
       if( Linea[1]=='!')
       {
         DosFamilias = true;
         fgets(Linea,100,Archivo);
       }
       Escribe_Al_Display("Copiando Archivos    Nuevos...          ");
       Contador = 0;
     }

     if (Contador=_CONTINUAR) continue; 

     //LEER LOS MODELOS
     if (linea=="") continue;
     Inc(Contador);

     if (! DosFamilias) ModelArchi = Linea else
      strcpy(ModelArchi, Copy(Linea,1,strlen(Linea)-2)); 

     //COPIAR EL ARCHIVO
     if (! CopiaModelo(ModelArchi))
     {
        fclose(Archivo); Error = 1; exit(0);
     }

     if (Contador % 5 == 0)
     {
        Monitor.posiciona(#32); if (Contador && 1 > 0)
         Monitor.EscribeMensaje("Espere...")
          else Monitor.EscribeMensaje("         ");
     }

   }

   Monitor.EscribeMensaje("Listo....");
   fclose(Archivo);
 }

{

  CopiaArchivosDeModelos = false;
  
   /*if ((NYCEL=_TRUE) && (Pos("CEL",Version)==1))
  {
    getDir(3,camino); Error=1;
    if (CopiaArchivoDeA("modelos.dat","a:\",camino,mensaje))
     Error=0; getdir(3,camino);
    if (CopiaArchivoDeA("numeros.lst","a:\",camino,mensaje))
     Error=0;
    if (Error==0) CopiaArchivosDeModelos=true; exit(0);
  };*/

  //DETERMINAR LA RUTA
  getdir(0,Ruta); //Definir
  if (Ruta[strlen(Ruta)]!='\') 
      strcat(Ruta,'\');

  if (FSearch(strcat(Ruta,"pkunzip.exe",Ruta)!="") //
   || (FSearch(strcat(Drive,"pkunzip.exe",Drive)!=""))
    && Exen(strcat("copy ",strcat(Drive,strcat("pkunzip.exe ",Ruta))))
     && (FSearch(strcat(Drive,"tablas.zip",Drive))!="")
      && (FSearch(strcat(Drive,"iosala.zip"),Drive)!="") &&
       Exen(strcat("copy ",strcat(Drive,"tablas.zip tablas.zip"))) &&
        Exen(strcat("copy ",strcat(Drive,"iosala.zip iosala.zip"))))
  {
     Escribe_Al_Display("Verificando Archivos Anteriores...     ");
     CreaDirSinoExiste("TABLAS");
     CreaDirSinoExiste("IOSALA");
     if (FSearch(strcat(Drive,"seclips.zip"),Drive)!="")
      CreaDirSinoExiste("SECLIP");
     Escribe_Al_Display("Borrando Archivo     Modelos.dat...    ");
     BorraDir(strcat(Ruta,"Modelos.dat"));
     Escribe_Al_Display("Borrando Archivos    TABLAS\*.tab...   ");
     BorraDir(strcat(Ruta+"TABLAS\*.tab"));
     Escribe_Al_Display("Borrando Archivos    IOSALA\*.ala...   ");
     BorraDir(strcat(Ruta,"IOSALA\*.ala"));
     if (FSearch(strcat(Drive,"seclips.zip"),Drive)!="")
     {
       Escribe_Al_Display("Borrando Archivos    SECLIP\*.scl...   ");
       BorraDir(strcat(Ruta,"SECLIP\*.scl"));
     }
     //COPIAR EL ARCHIVO MODELOS.DAT
     if (! CopiaArchivoDeX("modelos.dat"
      ,Drive,Ruta,mensaje)) exit(0); 
     Escribe_Al_Display("Descomprimiendo      Tablas.zip...     ");
     swap();
     system(strcat(Ruta+"pkunzip.exe"),strcat(" tablas.zip ",strcat(Ruta,"TABLAS\")));
     system(getenv("COMSPEC"),"/C cls");
     swap(); 
     if (errno==0)
     {
        Escribe_Al_Display("Descomprimiendo      Iosala.zip...     ");
        swap();
        system(strcat(Ruta,"pkunzip.exe"),strcat(" iosala.zip ",strcat(Ruta,"IOSALA\")));
        system(getenv("COMSPEC"),"/C cls"); 
        swap();
        if ((DosError=0) &&
         (FSearch(strcat(Drive,"seclips.zip"),Drive)!=""))
        {
           Escribe_Al_Display("Descomprimiendo      Seclip.zip...     ");
           swap();
           system(strcat(Ruta,"pkunzip.exe"),strcat(" seclip.zip ",strcat(Ruta,"SECLIP\\")));
           system(getenv("COMSPEC"),"/C cls");
           swap(); 
        }
     }
     Escribe_Al_Display("Borrando Archivos    Comprimidos...    ");
     BorraDir(strcat(Ruta,"tablas.zip"));
     BorraDir(strcat(Ruta,"iosala.zip"));
     BorraDir(strcat(Ruta,"seclip.zip"));
     if (UsaResetTecla)
     {
        setfillstyle(1,8);
        bar(5,INICIO_POSY-50,GetMaxX-5,INICIO_POSY+200);
        setfillstyle(solidfill,black);
        bar(12,INICIO_POSY-5,GetMaxX-10,INICIO_POSY+65);
        monitor.x=INICIO_POSX;
        monitor.y=INICIO_POSY;
        monitor.index=0;
        MainMenuBtn[1].cad="  S";
        MainMenuBtn[2].cad=" Izq";
        MainMenuBtn[3].cad=" Arr";
        MainMenuBtn[4].cad=" Der";
        MainMenuBtn[5].cad=" Rst";
        MainMenuBtn[1].Inicializa(290,220,35,34);
        MainMenuBtn[2].Inicializa(327,220,35,34);
        MainMenuBtn[3].Inicializa(364,220,35,34);
        MainMenuBtn[4].Inicializa(401,220,35,34);
        MainMenuBtn[5].Inicializa(438,220,35,34);
     }
     CopiaArchivosDeModelos=true;
     exit(0);
  }

  //CARGA EN UNA VARIABLE LOS MODELOS A COPIAR
  CargaModelosAVariable();

  if (errno!=0) exit(0);

  //COPIAR EL ARCHIVO MODELOS.DAT 
  if (! CopiaArchivoDeX("modelos.dat"
   ,Drive,Ruta,mensaje)) exit(0);

  CopiaArchivosDeModelos=true;

}

//=========================================================================
//=========================================================================
//=========================================================================
/*
void AcoplaArchivos()
{
 TSearchRec DirInfo;
 File *Archivo1,*Archivo2; 
 File *F;
 int i;
 char *linea;
 
 findfirst(strcat(camino,"*.ACD"), Archive, DirInfo); 
 while (errno == 0) 
 {
   linea="";
   //ABRIR EL ARCHIVO A ACOPLAR
   Archivo1=fopen(strcat(camino,DirInfo.name),"w+"); 
   //OBTENER EL NOMBRE DEL ARCHIVO AL QUE HAY QUE ACOPLARLO Y ABRIRLO
   fgets(linea,100,Archivo1);
   Archivo2=fopen(strcat(camino,linea),"rb+");
    if (errno!=0)
     {
      exit(0); 
      fclose(archivo1);
     }
   //EMPEZAR EL ACOPLAMIENTO
   while(! feof(Archivo1))
    {
	fgets(Linea,Archivo1);
	fputs(Linea,Archivo2);
    }

   fclose(Archivo1); fclose(Archivo2);
   remove(strcat(camino,DirInfo.name));
   //OPTENER OTRO ARCHIVO
   findnext(DirInfo); 
 }
  //BORRAR LOS ARCHIVOS CON EXTENSION ACD
  BorraDir("d:\*.acd");
}
*/
//==============================================================

bool HayError(int Error;char *Mensaje)
{
   HayError=(Error!=0);
   switch(Error)
   {
        case 2: strcat(Mensaje, "Archivo no encontrado"); break;
        case 3: strcat(Mensaje, "Path Incorrecto"); break;
        case 4: strcat(Mensaje, "Demasiados Archivo Abiertos"); break;
        case 5: strcat(Mensaje, "Acceso Negado"); break;
        case 101: strcat(Mensaje, "DISCO LLENO"); break;
        case 150: strcat(Mensaje, "CON PROTECCION"); break;
        case 152: strcat(Mensaje, "No Listo"); break;
        case 157: strcat(Mensaje, "Medio no reconocido"); break;
        case 158: strcat(Mensaje, "Sector no encontrado"); break;
       else strcat(Mensaje," Media Error"); break;
   }
}

//==============================================================

bool CopiaArchivoDeX(char *nombre, char *fuente, char *destino; char *mensaje);
{
  char Buff[3048];
  unsigned short int NumWritten; 
  File *FromF,*ToF; 
  bool Error;
  unsigned short int NumRead; 
  int Ic;

  //GetMem(Buff,2048);
  CopiaArchivoDeX=true;

  strcpy/(Mensaje,"Flopy ");

  for(Ic=1;IC<=strlen(nombre);Ic++)
   nombre[Ic]=toupper(nombre[Ic]); 

  if (fuente[strlen(fuente)]!='\')
   strcat(fuente,'\');
   
   FromF=fopen(strcat(fuente,nombre),"W+");
  Reset(FromF,1); //Pendiente

  if (! HayError(IORESULT,mensaje)) 
  {

    if (destino[strlen(destino)]!='\')
      strcat(destino,'\');

    if (Pos(".TAB",nombre)>0) 
     strcat(destino,"TABLAS\");

    if (Pos(".ALA",nombre)>0)
     strcat(destino,"IOSALA\");

    if (Pos(".SCL",nombre)>0)
     strcat(destino,"SECLIP\");
	 
	ToF=fopen(strcat(destino+nombre),"r+"); 
    Rewrite(ToF,1); // CREAR EL ARCHIVO 

    if (! HAyError(IORESULT,Mensaje)) 
    {

      Str(FileSize(FromF),mensaje); 

      //Escribe_Al_Display(strcat("Copiando ",strcat(mensaje," bytes...")));
      do{
        strcpy(Mensaje,"Flopy ")
		fread(NumRead, sizeof(buff),sizeof(buff),FromF);
        fread(FromF, buff , sizeof(buff), NumRead); //Definir
        Error= HayError(IORESULT,mensaje);
        if (! error)
        {
		  fwrite(NumRead, sizeof(buff),sizeof(buff),ToF);
          Mensaje="Flash";
          Error= HayError(IORESULT,mensaje);
        }
      }while((NumRead == 0) || (NumWritten != NumRead) || Error);

      //SI NO HAY ERROR ENTONCES CERRAR LOS ARCHIVOS
      if (! Error)
      {
        fclose(FromF);
        Error= HayError(IORESULT,mensaje);
        if (! Error)
        {
          fclose(ToF);
          Error=HayError(IORESULT,Mensaje);
        }
      }

    }

  }

  if (Error) CopiaArchivoDeX=false;
}
//end.